//Erica Nwoga
//Enlighten - Alphabet Soup Coding Challenge

//nodejs file system module 
const fs = require('fs');
let input = ""; //input string

//read the file, you may assume input files are correctly formatted
fs.readFile('input.txt', 'utf8', (e,data)=>{
    if(e){
        //an error occured
        console.error(e);
        return;
    }

    //store the input into a variable
    input = `${data}`;
    main(input);
});

//execute the game
function main(){
    input = input.split('\r\n'); //sepreate by line into array

    //The first line contains the dimensions
    let dim  = input[0].split('x')
    let rows = Number(dim[0])
    let cols = Number(dim[1])

    //Based on dimensions, organize chars into 2D array
    let grid = [] //represnents the grid
    
    //Lines 1 to row, represents our grid
    for(let i = 1; i <= rows; i++ ){
        //convert each line into an array and add to grid
        grid.push(input[i].split(' '))
    }

    //The remaining lines are words to be found
    let hiddenWords = []
    //collect the hidden words into an array
    for(let i = rows + 1; i < input.length; i++){
        hiddenWords.push(input[i]);
    }

    let coordinates = "" //will hold the start and end locations

    //For each hiddenWord, we search for it
    for(let i = 0; i < hiddenWords.length; i++){
        //find the coordinates starting at the first letter
        coordinates = searchAtLetter(hiddenWords[i], rows, cols, grid);

        //output our files to console 
        console.log(`${hiddenWords[i]} ${coordinates}`);
    }
}

//searches for the word using the FIRST letter, returns the start and end points
function searchAtLetter(word, rows, cols, grid){
    let vCheck =""; //checks if the word exist vertically
    let hCheck =""; //checks if the word exist horizontally
    let dnegCheck =""; // checks diagnal with \ negative slope
    let dposCheck =""; //checks diagonal with / positive slope


    let endLocation = "";

    for(let r = 0; r < rows; r++){
        for(let c = 0; c < cols; c++){
            if(grid[r][c] == word[0]){
                //there is a possible match, immediatly search the grid in all directions
                vCheck = checkVertical(word, r, c, rows, grid);
                hCheck = checkHorizontal(word, r, c, cols, grid);
                dnegCheck = checkDiagnonalNeg(word, r, c, rows, cols, grid);
                dposCheck = checkDiagnonalPos(word, r, c, rows, cols, grid);

                //Confirms the direction the word was found in
                if(vCheck != "none"){
                    //word was found vertically
                    endLocation = vCheck
                }else if(hCheck != "none"){
                    //word was found horizontally
                    endLocation = hCheck
                }else if(dnegCheck != "none"){
                    //word was found along negative slope
                    endLocation = dnegCheck
                }else if(dposCheck != "none"){
                    //word was found along positive slope
                    endLocation = dposCheck
                }else{
                    //all checks equal none, so this is NOT the starting point
                    endLocation = "none"
                }

                //returns the start and end coordinates as a string
                if(endLocation != "none"){
                    return `${r}:${c} ${endLocation}`;
                }
            }
        }
    }
} 

//Check functions validate if the word is found, returns end coordniate, else none

//Checks for the word upwards and downwards
function checkVertical(word, row, col, totalRows, grid){
    let isvalid = true; //flag
    let dir = "forwards" //direction
    let j = 0; //indexes through word

    //first check if word can even fit
    if(row + word.length <= totalRows || row - word.length >= -1){
        //check the direction in which to search

        if(row == 0){  //if it starts at the top 
            //search downwards
            dir = "forwards"
            for(let r = row; j < word.length; r++){
                if(grid[r][col] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else if(row == totalRows - 1){  //if it starts at the bottom 
            //search upwards
            dir = "backwards"
            for(let r = row; j < word.length; r--){
                if(grid[r][col] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }
        else if(word[1] == grid[row + 1][col]){  //if the letter below matches
            //search downwards             
            dir = "forwards"
            for(let r = row; j < word.length; r++){
                if(grid[r][col] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }

        }
        else if(word[1] == grid[row - 1][col]){  //if the letter above matches
            //search upwards
            dir = "backwards"
            for(let r = row; j < word.length; r--){
                if(grid[r][col] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }

        }else{
            //only one letter matched
            isvalid = false
        }
    }else{
        //word does not fit in grid, so automatically false
        isvalid = false;
    }

    if(isvalid == true){
        //word found vertically 
        if(dir == "forwards"){
            return `${row + word.length - 1}:${col}`
        }else{
            return `${row - word.length + 1}:${col}`
        }
    }else{
        return "none" //no matches
    }
}

//Checks for the word forwards and backwards 
function checkHorizontal(word, row, col, totalCols, grid){
    isvalid = true; //flag
    let dir = "forwards" //direction
    let j = 0; //indexes through word

    //first check if word can even fit
    if(col + word.length <= totalCols || col - word.length >= -1){
        //check the direction in which to search

        if(col == 0){  //if it starts at the left
            dir = "forwards"
 
            //search to the right
            for(let i = col; j < word.length; i++){
                if(grid[row][i] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else if(col == totalCols - 1){  //if it starts at the right 
            //search backwards
            dir = "backwards"

            for(let i = col; j < word.length; i--){
                if(grid[row][i] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }
        else if(word[1] == grid[row][col + 1]){  //if the letter on the right matches
            //search to the right
            dir = "forwards"

            for(let i = col; j < word.length; i++){
                if(grid[row][i] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }
        else if(word[1] == grid[row][col - 1]){  //if the letter before matches
            //search backwards
            dir = "backwards"

            for(let i = col; j < word.length; i--){
                if(grid[row][i] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else{
            //only one letter matched
            isvalid = false
        }

    }else{
        //word does not fit in grid, so automatically false
        isvalid = false;
        return "none";
    }


    if(isvalid == true){
        //word found horizontally 
        if(dir == "forwards"){
            return `${row}:${col + word.length - 1}`
        }else{
            return `${row}:${col - word.length + 1}`
        }
    }else{
        return "none" //no matches
    }
}

//Checks the word on a negative slope
function checkDiagnonalNeg(word, row, col, totalRows, totalCols, grid){
    isvalid = true; //flag
    let dir = "forwards" //direction
    let j = 0; //indexes through word

    //first check if word can even fit
    if(word.length <= totalRows && word.length <= totalCols){
        //check the direction in which to search

        if(row == 0 || col == 0){  //if word starts on top row or leftmost col 
            dir = "forwards"
 
            //We can ONLY search to the right-downwards
            for(let x = row, y = col; j < word.length; x++, y++){
                //if it indexes out of bounds
                if(x > totalRows - 1 || y > totalCols - 1){
                    isvalid = false;
                    break;
                }
                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else if(row == totalRows - 1 || col == totalCols - 1){  //if word starts on bottom row or last col 
            dir = "backwards"
 
            //We can ONLY search to the left-upwards
            for(let x = row, y = col; j < word.length; x--, y--){
                //if it indexes out of bounds
                if(x < 0 || y < 0){
                    isvalid = false;
                    break;
                }

                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word

                
            }
        } else if(word[1] == grid[row + 1][col + 1]){  //if the letter after matches
            //search to the right
            dir = "forwards"

            //search to the right-downwards
            for(let x = row, y = col; j < word.length; x++, y++){
                //if it indexes out of bounds
                if(x > totalRows - 1 || y > totalCols - 1 ){
                    isvalid = false;
                    break;
                }

                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }
        else if(word[1] == grid[row - 1][col - 1]){  //if the letter before matches
            //search backwards
            dir = "backwards"

            //search to the left-upwards
            for(let x = row, y = col; j < word.length; x--, y--){
                //if it indexes out of bounds
                if(x < 0 || y < 0){
                    isvalid = false;
                    break;
                }
                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else{
            //only one letter matched
            isvalid = false
        }

    }else{
        //word does not fit in grid, so automatically false
        isvalid = false;
        return "none";
    }


    if(isvalid == true){
        //word found horizontally 
        if(dir == "forwards"){
            return `${row + word.length - 1}:${col + word.length - 1}`
        }else{
            return `${row - word.length + 1}:${col - word.length + 1}`
        }
    }else{
        return "none" //no matches
    }
}

//Checks the word on a positive slope
function checkDiagnonalPos(word, row, col, totalRows, totalCols, grid){
    isvalid = true; //flag
    let dir = "forwards" //direction
    let j = 0; //indexes through word

    //first check if word can even fit
    if(word.length <= totalRows && word.length <= totalCols){
        //check the direction in which to search
        if(row == totalRows - 1 || col == 0){  //if word starts on bottom row or first col 
            dir = "forwards"
 
            //We can ONLY search to the right-upwards
            for(let x = row, y = col; j < word.length; x--, y++){

                //if it indexes out of bounds
                if(x < 0 || y > totalCols - 1){
                    isvalid = false;
                    break;
                }
                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else if(row == 0 || col == totalCols - 1){  //if word starts on top row or last col 
            dir = "backwards"

            //We can ONLY search to the left-downwards
            for(let x = row, y = col; j < word.length; x++, y--){

                //if it indexes out of bounds
                if(x > totalRows - 1 || y < 0){
                    isvalid = false;
                    break;
                }

                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word

                
            }
        } else if(word[1] == grid[row - 1][col + 1]){  //if the letter above matches
            //search to the right
            dir = "forwards"             

            //search to the right-downwards
            for(let x = row, y = col; j < word.length; x--, y++){
                //if it indexes out of bounds
                if(x < 0 || y > totalCols - 1 ){
                    isvalid = false;
                    break;
                }

                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }
        else if(word[1] == grid[row + 1][col - 1]){  //if the letter below matches
            //search backwards
            dir = "backwards" 

            //search to the left-downwards
            for(let x = row, y = col; j < word.length; x++, y--){
                //if it indexes out of bounds
                if(x > totalRows - 1 || y < 0){
                    isvalid = false;
                    break;
                }
                if(grid[x][y] != word[j]){
                    isvalid = false;
                }
                j++; //look at next letter in the word
            }
        }else{
            //only one letter matched
            isvalid = false
        }

    }else{
        //word does not fit in grid, so automatically false
        isvalid = false;
        return "none";
    }


    if(isvalid == true){
        //word found horizontally 
        if(dir == "forwards"){
            return `${row - word.length + 1}:${col + word.length - 1}`
        }else{
            return `${row + word.length - 1}:${col - word.length + 1}`
        }
    }else{
        return "none" //no matches
    }
}